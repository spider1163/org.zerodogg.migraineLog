// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2024   Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'genericwidgets.dart';
import 'datatypes.dart';

class MigraineLogConfigScreen extends StatelessWidget
    with MigraineLogConfigScreenStrings {
  String _generalMessage() {
    return Intl.message(
      'General',
      desc:
          'Used as a header in the config screen to refer to "general settings"',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(configHeaderMessage()),
      ),
      // The main editor, with the date, strength and meds taken selectors
      body: SingleChildScrollView(
        child: Column(
          children: [
            MigraineLogHeader(text: _generalMessage()),
            Consumer<MigraineLogConfig>(
              builder: (context, MigraineLogConfig config, _) =>
                  MigraineLogNeutralLanguageConfig(config: config),
            ),
            SizedBox(height: 10),
            Consumer<MigraineLogConfig>(
              builder: (context, MigraineLogConfig config, _) =>
                  MigraineLogNoteMarkerConfig(config: config),
            ),
            SizedBox(height: 10),
            Consumer<MigraineLogConfig>(
              builder: (context, MigraineLogConfig config, _) =>
                  MigraineLogAuraStrengthConfig(config: config),
            ),
            SizedBox(height: 10),
            Consumer<MigraineLogConfig>(
              builder: (context, MigraineLogConfig config, _) =>
                  MigraineLogNoHeadacheConfig(config: config),
            ),
            MigraineLogHeader(text: medicationsMessage()),
            MDTextBox(text: medChangeInfo()),
            Consumer<MigraineLogConfig>(
              builder: (context, MigraineLogConfig config, _) =>
                  ConfigMedications(medications: config.medications),
            )
          ],
        ),
      ),
    );
  }
}

class MigraineLogFirstTimeConfig extends StatelessWidget
    with MigraineLogConfigScreenStrings {
  String _continueString() {
    return Intl.message(
      "Continue",
      desc: "Used as a button in the 'first time' configuration screen",
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(firstTimeConfigHeaderMessage()),
      ),
      // The main editor, with the date, strength and meds taken selectors
      body: SingleChildScrollView(
        child: Column(
          children: [
            Consumer<MigraineLogConfig>(
              builder: (context, MigraineLogConfig config, _) =>
                  MigraineLogNeutralLanguageOnboarding(config: config),
            ),
            MigraineLogHeader(text: medicationsMessage()),
            MDTextBox(text: firstTimeMessage()),
            Consumer<MigraineLogConfig>(
              builder: (context, MigraineLogConfig config, _) =>
                  ConfigMedications(medications: config.medications),
            )
          ],
        ),
      ),
      persistentFooterButtons: [
        Consumer<MigraineLogConfig>(
            builder: (context, config, _) => TextButton(
                child: Text(_continueString()),
                onPressed: () {
                  config.onboardingDone(1);
                }))
      ],
    );
  }
}

class MigraineLogNeutralLanguageConfig extends StatelessWidget {
  MigraineLogNeutralLanguageConfig({required this.config});
  final MigraineLogConfig config;

  String _neutralLanguageText() {
    return Intl.message('Use neutral language',
        desc:
            'Toggle button that enables (or disables) use of "headache" instead of "migraine"');
  }

  String _neutralLanguageSubtitle() {
    return Intl.message('Use the term "headache" rather than "migraine"');
  }

  @override
  Widget build(BuildContext contect) {
    return SwitchListTile.adaptive(
      title: Text(_neutralLanguageText()),
      subtitle: Text(_neutralLanguageSubtitle()),
      value: config.neutralLanguage,
      onChanged: (state) => config.neutralLanguage = state,
    );
  }
}

class MigraineLogAuraStrengthConfig extends StatelessWidget {
  MigraineLogAuraStrengthConfig({required this.config});
  final MigraineLogConfig config;

  String _auraStrengthText() {
    return Intl.message('Enable "aura only" strength',
        desc:
            'Toggle button that enables (or disables) use of "aura only" as a "strength" option');
  }

  String _auraStrengthSubtitle() {
    return Intl.message('Let me set "aura only" as a the strength of an attack',
        desc:
            "This is used as a description for the 'aura only' config option. Feel free to rephrase.");
  }

  @override
  Widget build(BuildContext contect) {
    return SwitchListTile.adaptive(
      title: Text(_auraStrengthText()),
      subtitle: Text(_auraStrengthSubtitle()),
      value: config.enableAuraStrength,
      onChanged: (state) => config.enableAuraStrength = state,
    );
  }
}

class MigraineLogNoHeadacheConfig extends StatelessWidget {
  MigraineLogNoHeadacheConfig({required this.config});
  final MigraineLogConfig config;

  String _auraStrengthText() {
    return Intl.message('Enable notes on days without attacks',
        desc:
            'Toggle button that enables (or disables) use of "no headache" as a "strength" option');
  }

  String _auraStrengthSubtitle() {
    return Intl.message(
        'Lets you choose a "no headache" as the strength of an attack',
        desc:
            "This is used as a description for the 'no headache' config option. Feel free to rephrase.");
  }

  @override
  Widget build(BuildContext contect) {
    return SwitchListTile.adaptive(
      title: Text(_auraStrengthText()),
      subtitle: Text(_auraStrengthSubtitle()),
      value: config.enableNoHeadacheStrength,
      onChanged: (state) => config.enableNoHeadacheStrength = state,
    );
  }
}

class MigraineLogNoteMarkerConfig extends StatelessWidget {
  MigraineLogNoteMarkerConfig({required this.config});
  final MigraineLogConfig config;

  String _noteMarkerText() {
    return Intl.message('Show note indicators',
        desc:
            "Toggle button that enables/disables showing a marker on dates with notes in the calendar");
  }

  String _noteMarkerSubtitle() {
    return Intl.message(
        'Show a small dot on dates where you have written a note',
        desc:
            "The 'dot' is a small grey square, displayed in the upper right hand corner of the date number in the calendar");
  }

  @override
  Widget build(BuildContext contect) {
    return SwitchListTile.adaptive(
      title: Text(_noteMarkerText()),
      subtitle: Text(_noteMarkerSubtitle()),
      value: config.displayNoteMarker,
      onChanged: (state) => config.displayNoteMarker = state,
    );
  }
}

class MigraineLogNeutralLanguageOnboarding extends StatelessWidget {
  MigraineLogNeutralLanguageOnboarding({required this.config});
  final MigraineLogConfig config;

  @visibleForTesting
  String headacheTypeHeader() {
    return Intl.message('What kind of headache do you have?');
  }

  @visibleForTesting
  String otherDescr() {
    return Intl.message('Select this if you\'re unsure');
  }

  @visibleForTesting
  String migraineMessage() {
    return Intl.message('Migraine');
  }

  @visibleForTesting
  String otherMessage() {
    return Intl.message('Other (general headaches, cluster etc.)');
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      MigraineLogHeader(text: headacheTypeHeader()),
      RadioListTile<bool>.adaptive(
        title: Text(migraineMessage()),
        value: false,
        groupValue: config.neutralLanguage,
        onChanged: (val) => config.neutralLanguage = val!,
      ),
      RadioListTile<bool>.adaptive(
        title: Text(otherMessage()),
        subtitle: Text(otherDescr()),
        value: true,
        groupValue: config.neutralLanguage,
        onChanged: (val) => config.neutralLanguage = val!,
      ),
    ]);
  }
}

mixin MigraineLogConfigScreenStrings {
  String configHeaderMessage() {
    return Intl.message(
      'Settings',
      desc: "The name of the app configuration screen",
    );
  }

  String firstTimeConfigHeaderMessage() {
    return Intl.message(
      'Configuration',
      desc:
          "The name of the first time configuration screen. This is pretty much like the settings screen, but with some different information and displayed only once, therefore using a different name to make this clearer.",
    );
  }

  String medicationsMessage() {
    return Intl.message('Medication list',
        desc:
            'Header for the part of the configuration screen where the user sets up their medication');
  }

  String medChangeInfo() {
    return Intl.message(
        'Changing the list of medications here will not change medications in entries you have already added.');
  }

  String firstTimeMessage() {
    return Intl.message(
        "If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.");
  }
}

class ConfigMedications extends StatelessWidget {
  ConfigMedications(
      {required this.medications, this.isFirstTimeConfig = false});
  final MigraineMedicationList medications;
  final bool isFirstTimeConfig;

  @override
  Widget build(BuildContext context) {
    List<Widget> entries = [];
    for (var entry in medications.listWithEmpty) {
      entries.add(ConfigMedicationsEntry(
        key: entry.key,
        list: medications,
        entry: entry,
      ));
    }
    return SizedBox(
      height: 400,
      child: ReorderableListView(
          onReorder: (int oldIdx, int newIdx) =>
              medications.reorderByIndex(oldIdx, newIdx),
          children: entries),
    );
  }
}

class ConfigMedicationsEntry extends StatefulWidget {
  ConfigMedicationsEntry({
    Key? key,
    required this.entry,
    required this.list,
  }) : super(key: key);
  final MigraineMedicationEntry entry;
  final MigraineMedicationList list;

  @override
  _ConfigMedicationsEntryState createState() => _ConfigMedicationsEntryState();
}

class _ConfigMedicationsEntryState extends State<ConfigMedicationsEntry> {
  final _controller = TextEditingController();
  bool isEmpty = false;

  String _emptyHintText() {
    return Intl.message('Add a new medication');
  }

  @override
  void initState() {
    super.initState();
    if (widget.entry.medication == "") {
      isEmpty = true;
    }
    _controller.text = widget.entry.medication;
    _controller.addListener(() {
      widget.entry.medication = _controller.text;
      if (_controller.text != '') {
        if (isEmpty) {
          widget.list.add(widget.entry);
          isEmpty = false;
        } else {
          /// Clean up the list if needed
          widget.list.removeEmpty();
        }
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  String _undoStr() {
    return Intl.message('Undo');
  }

  String _deletedElement(String medication) {
    return Intl.message(
      "Deleted \"$medication\"",
      args: [medication],
      name: "_deletedElement",
      desc:
          "A pop-up message after the user has deleted a medication. Is accompanied with an 'undo' button.",
    );
  }

  @override
  Widget build(BuildContext context) {
    assert(widget.list.has(widget.entry) || isEmpty);

    Widget icon = Container();
    if (!isEmpty) {
      icon = Icon(Icons.drag_handle);
    }
    final Widget element = Padding(
      padding: EdgeInsets.only(
        left: MDTextBox.defaultInsets,
        right: MDTextBox.defaultInsets,
        bottom: MDTextBox.defaultInsets,
      ),
      child: Row(children: [
        Expanded(
            child: TextField(
          controller: _controller,
          keyboardType: TextInputType.text,
          minLines: 1,
          maxLines: 1,
          decoration: InputDecoration(
            hintText: isEmpty ? _emptyHintText() : '',
            border: UnderlineInputBorder(),
          ),
        )),
        Padding(
          padding: EdgeInsets.only(right: MDTextBox.defaultInsets),
          child: icon,
        ),
      ]),
    );
    var confirmDismiss;
    var background = Container(color: Colors.red);
    var direction = DismissDirection.horizontal;
    // Don't allow dismissing if this is the placeholder "add" entry.
    // This implementation is quite hacky. If we don't add our Dismissible to
    // the tree, then it'll rebuild the entire tree, losing focus in the
    // text input. Therefore we always add the Dismissible to our tree, but we
    // ignore events on it.
    if (widget.entry.medication == "") {
      // Ignore all attempts to dismiss
      confirmDismiss = (_) async => false;
      // Add an empty background
      background = Container();
      // Limit the direction a bit
      direction = DismissDirection.startToEnd;
    }
    return Dismissible(
        key: widget.entry.key,
        confirmDismiss: confirmDismiss,
        background: background,
        direction: direction,
        onDismissed: (_) {
          var entry = widget.entry;
          var list = widget.list;
          var idx = list.remove(entry);
          // Allow undo *if* this entry was non-empty
          if (entry.medication != "") {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              duration: Duration(seconds: 6),
              content: Text(_deletedElement(entry.medication)),
              action: SnackBarAction(
                label: _undoStr(),
                onPressed: () => list.add(entry, position: idx),
              ),
            ));
          }
        },
        child: element);
  }
}
