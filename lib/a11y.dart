// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2022    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';

/// Builds a TableRow widget with `elements` as its children, where each
/// element gets wrapped in a Semantics, where the first element has semantics
/// enabled, and the last element has semantics excluded. The end result is
/// that only the first column gets seen by the semantic engine, and thus seen
/// by ie. screen readers. Combine this with a semanticsLabel on the first
/// column containing the text for the entire row and the result is that screen
/// readers will be able to read the entire row in one go, much more naturally
/// than if each column gets read individually.
///
/// This is a stopgap measure until there's some way to MergeSemantics on table
/// rows.
TableRow buildAccessibleTableRow({required List<Widget> elements}) {
  var first = true;
  List<Widget> builtElements = [];
  for (var entry in elements) {
    builtElements
        .add(Semantics(value: '', excludeSemantics: !first, child: entry));
    first = false;
  }
  return TableRow(children: builtElements);
}
