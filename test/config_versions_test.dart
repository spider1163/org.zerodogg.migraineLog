// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2022   Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:io';
import 'package:MigraineLog/datatypes.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'utils.dart';
import 'dart:convert';

class MockMigraineList extends Mock implements MigraineList {}

class DelayedDataWriteTester with DelayedDataWrite {
  int writeCalls = 0;

  @override
  void write() {
    writeCalls++;
  }
}

class SafeDataWriteTester with SafeDataWrite {}

void main() {
  // Needed for the getApplicationDocumentsDirectory mock to work.
  TestWidgetsFlutterBinding.ensureInitialized();

  Directory? tmp;

  setUp(() async {
    tmp = await Directory.systemTemp.createTemp();
    PathProviderPlatform.instance = MockPathProviderPlatform(dir: tmp);
    return true;
  });
  tearDown(() async {
    tmp!.deleteSync(recursive: true);
    tmp = null;
    return true;
  });

  group('version 1', () {
    test('base', () async {
      var c = MigraineLogConfig();
      var cfile = await c.configFilePath();
      expect(await cfile.exists(), isFalse);
      await c.loadConfig(); // Should succeed even when there's no file
      await cfile.writeAsString(jsonEncode({
        "onboardingVersion": 1,
        "medication": ["Testit"],
        "neutralLanguage": true,
        "configVersion": 1,
      }));
      await c.loadConfig();
      expect(c.neutralLanguage, isTrue);
      expect(c.onboardingVersion, 1);
      expect(c.displayNoteMarker, isFalse);
      expect(c.medications.has(MigraineMedicationEntry(medication: "Testit")),
          isTrue);
    });
    test('with displayNoteMarker', () async {
      var c = MigraineLogConfig();
      var cfile = await c.configFilePath();
      expect(await cfile.exists(), isFalse);
      await c.loadConfig(); // Should succeed even when there's no file
      await cfile.writeAsString(jsonEncode({
        "onboardingVersion": 1,
        "medication": ["Testit"],
        "neutralLanguage": true,
        "displayNoteMarker": true,
        "configVersion": 1,
      }));
      await c.loadConfig();
      expect(c.neutralLanguage, isTrue);
      expect(c.onboardingVersion, 1);
      expect(c.medications.has(MigraineMedicationEntry(medication: "Testit")),
          isTrue);
      expect(c.displayNoteMarker, isTrue);
    });
  });
}
