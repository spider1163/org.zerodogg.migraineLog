# Changelog
Alle viktige endringar til dette prosjektet vil bli dokumentert i denne fila.

Formatet er basert på [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
og prosjektet følg [semantiske versjonar](https://semver.org/spec/v2.0.0.html).

Dette er den norske endringsloggen, som skal speile den engelske frå
ChangeLog.md.

## [0.11.2] - 2024-03-19

### Fiksa
- Dagar med "ingen hovudpine" som feilaktig vart telt som hovudpinedagar

## [0.11.1] - 2024-02-16

### Fiksa
- Bygging utan "ios" mappa (for F-droid)

## [0.11.0] - 2024-02-11

### Lagt til
- Ein funksjon som let deg slå på høvet til å leggje til notat på dagar utan
    anfall
- Europeisk Portugisisk og Russisk omsetjing

### Endra
- Oppdatert Flutter til 3.16
- Forbetra integrering av brukargrensesnitt på iOS

### Fiksa
- Grafiske feil på enkelte LineageOS-einingar

## [0.10.5] - 2024-01-27

### Endra
- Oppdaterte omsetjingar for Baskisk og Tysk
- Bruk VectorDrawable ikon på Android (takk til Sébastien Delord)
- La til eit svart-kvitt ikon på Android (takk til Sébastien Delord)

## [0.10.4] - 2023-12-15

### Fiksa
- Ei ugyldig oppsettsfil vil ikkje lenger gjere at Migrenelogg henger ved oppstart

## [0.10.3] - 2023-11-05

### Fiksa
- Utrekning av statistikk i månadar kor ein har endra frå sommartid til vintertid

## [0.10.2] - 2023-09-24

### Endra
- Berre Android: fiksa ein feil som gjorde at importering ikkje fungerte på Android 13

## [0.10.1] - 2023-08-27

### Endra
- Berre Android: oppdater mål-SDK til 33
- Fiksa ymse skrivefeil på Engelsk

## [0.10.0] - 2023-03-04

### Endra
- Oppdatert til Flutter versjon 3.3.10

### Lagt til
- Baskisk omsetjing

## [0.9.1] - 2022-09-18

### Lagt til
- Fransk omsetjing

## [0.9.0] - 2022-08-07

### Endra
- Oppdatert til flutter versjon 3
- Finjustert storleiken på kalenderen på store skjermar (nettbrett)

### Lagt til
- iOS versjon

## [0.8.4] - 2022-07-30

### Endra
- La til Kinesisk (hans) omsetjing

## [0.8.3] - 2022-07-21

### Endra
- Oppdatert tysk omsetjing

## [0.8.2] - 2022-07-19

## Fiksa
- Engelske språkinnstillingar vert no respektert
- Eksportert html nyttar no korrekt grammatikk på finsk

## [0.8.1] - 2022-06-16

### Fiksa
- F-droid versjon

## [0.8.0] - 2022-06-12

### Lagt til
- Du kan no velje å registrere anfall kor du berre har hatt aura-symptom

### Endra
- Versjonen til dataformatet er no 2. Versjon 1 data kan framleis verta lest, men import av data frå denne versjonen vil krevje 0.8.0 eller seinare.

## [0.7.2] - 2021-06-09

### Fiksa
- Eit problem kor Migrenelogg ikkje alltid lagra endringane viss den einaste endringa som hadde blitt gjort var å slette ein oppføring

## [0.7.1] - 2021-05-20

### Endra
- Oppdatert spansk omsetjing

## [0.7.0] - 2021-05-09

### Lagt til
- Kan vise eit merke på datoar kor du har lagt til eit notat
- Du kan no velje å vise datoar som ikkje har oppføringar i den eksporterte HTML-fila

## [0.6.1] - 2021-04-07

### Endra
- Oppdatert tysk omsetjing

### Fiksa
- Korrigerte feilmeldinga som kjem når ein prøver å importere data frå ein nyare versjon av Migrenelogg

## [0.6.0] - 2021-03-23

### Endra
- Kan no gjenopprette data viss den vert korrupt (t.d. viss batteriet går tomt under lagring)
- Endringar for å gjere omsetting av appen enklare
- La til ei tysk omsetjing av jofrev
- La til ei spansk omsetjing av Diego

## [0.5.1] - 2021-03-18

### Fiksa
- Ein kan no scrolle i legg til/rediger skjermen sånn at «lagre»-knappen ikkje overlappar notat-feltet
- Ymse språk-korreksjonar

## [0.5.0] - 2021-03-16

### Lagt til
- Eit hjelp-vindauge som forklarar dei forskjellege styrkane

### Endra
- Ikonet er no eit «android adaptivt» ikon, dette vil løyse problem med korleis somme heimskjermar viser ikonet

## [0.4.0] - 2021-03-08

### Endra
- Du kan no halde nede på ein dato i kalenderen for å redigere eller leggje til ein oppføring på den datoen
- Vis namnet til eksterne omsetjarar i «om»-vindauget
- La til omsetjing der det mangla
- Tabelloverskriftar i eksporterte filer vil gjenta på kvar side viss ein tabell går over meir enn ei enkelt side
- Oppreinsking av koden

## [0.3.1] - 2021-03-06

### Lagt til
- Finsk omsetjing av Mika Latvala
- Metadata som er naudsynt for f-droid

## [0.3.0] - 2021-03-02

### Lagt til
- Støtte for å skjule og vise individuelle månadar i den eksporterte fila
- Støtte for å sortere tabellane i den eksporterte fila
- Støtte for å avgrense kor mange månader som vart vist i den eksporterte fila

### Fiksa
- Meldinga som kjem etter importering er no òg på Norsk

## [0.2.0] - 2021-02-27

### Lagt til
- Støtte for å importere data frå eksporterte filer

### Endra
- Eksportert data er no inndelt etter månad
- Eksportert data har no oppsummeringar, sånn som dei som er i statistikk-fana

### Fiksa
- Tooltips for fanelinja er no òg på Norsk

## [0.1.2] - 2021-02-24

### Fiksa
- Fiksa «hjelp»-vindauget

## [0.1.1] - 2021-02-23

### Lagt til
- La til lenkjer i «om»-vindauget

### Fiksa
- Fiksa høvet til å omsetje teksten for den valde månaden i kalenderen og
    statistikk-fana

## [0.1.0] - 2021-02-23
- Fyrste utgiving
